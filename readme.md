# Modulo para comunicação com equipamentos de medição de energia elétrica.

## 🎯 Objetivo

Este projeto tem como intuito a interpretação e comunicação com equipamentos de medição de energia elétrica.
Utilizando da norma brasileira de intercâmbio de informações NBR14522.

## 🛠️ Construído com

Lista de Módulos / Frameworks utilizados na construção do projeto:

* [PymodbusTCP]
* [Pebble]
* [Socket]
* [Struct]

## ✒️ Autores

Lucas Specht (https://gitlab.com/lucasspecht1)



