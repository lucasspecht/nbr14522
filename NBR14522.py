#############################################################################################################
#                                                                                                           #
#   Este Modulo tem como função a comunicação com medidores elétricos (SMF)                                 #
#   por meio do protocolo de comunicação NBR-14522                                                          #
#                                                                                                           #
#   Autor: Lucas Specht                                                                                     #
#                                                                                                           #
#   Versão: 1.3                                                                                             #
#                                                                                                           #
#   Última revisão: 24/10/2023                                                                              #
#                                                                                                           #
#############################################################################################################

#----------------------->  Dicionario de termos contidos neste script  <--------------------------#                                         
''' - HEX: Hexadecimal.                                                                     
    - Word: Cadeia de caracteres que representam uma informação, podendo ser HEX ou Bits.   
    - IEEE-754: Método de conversão de Binario (bits) para float.                   
    - CRC: 'Cyclic Redundancy Check' (Verificação Cíclica de Redundância), 
        - verificação de erros e ordenação de words, contidas na Cadeia Hex.                   ''' 
#-------------------------------------------------------------------------------------------------#          
                                     
#--------------------------> Breve Explicações de funcionamento <---------------------------------#
''' - O começo de uma word de comando e resposta sempre ira conter os valores: 0199 
    - O comando sempre tera o tamanho de 68 'Octetos' (Bytes) - (66 Octetos mais 2: (01, 99))
    - A resposta sempre tera o tamanho de 260 'Octetos' (Bytes) - (258 Octetos mais 2: (01, 99))
        - cada Octeto é representado em HEX com dois digitos.                                  '''
#-------------------------------------------------------------------------------------------------#
import socket
import struct
from pyModbusTCP.utils import crc16
from pebble import concurrent

class ComunicacaoNBR:

    @staticmethod
    def conexao_socket(ip: 'str', port: 'int')-> 'object | None':
        ''' Este método conecta a um local especifico 
        e retorna um objeto Socket para comunicação '''

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((ip, port))

            return sock
        
        except Exception as e:
            print(f'Não foi possivel efetuar a conexão com o medidor, erro: {e}')
            return 
    
    @classmethod
    def montar_hex_comando(cls, comando: 'int', leitora: 'int', parametro = '00')-> 'str':
        ''' Este método Concatena uma str contendo 
        comando junto do CRC e a retorna '''
    
        comando = f'0199{comando}{leitora}{parametro}'
        rep = 132 - len(comando)

        comando = f'{comando}{"0"*rep}'
        comando = cls.adcionar_crc(comando)

        return comando

    @classmethod
    @concurrent.process(timeout=20)
    def envio_comando(cls, conexao: 'socket', hex: 'str')-> 'bytes | None':
        ''' Este método envia um determinado comando baseado 
        no HEX recebido nos parametros e retorna a resposta '''

        try:
            bytes_cmd = bytes.fromhex(hex)
        
            conexao.sendall(bytes_cmd)
            resposta = conexao.recv(260).hex()

            while len(resposta) < 520:
                resposta += conexao.recv(260).hex()

            return resposta
        
        except Exception as e:
            print(f'Não foi possivel se comunicar com o destino, ERRO: {e}')
            return

    @staticmethod
    def adcionar_crc(data: 'str')-> 'str':
        ''' Este método adciona o CRC ao fim do comando e 
        o retorna por completo contendo o CRC no final '''

        crc = hex(crc16(bytes.fromhex(data)))  #-> Calcula o crc do comando utilizando o método crc16 
                                               # do Pymodbus e utilizando o HEX completo
        crc = crc.split('x')[1] 
        crc = crc if len(crc) > 3 else f'0{crc}'

        crc_str = crc[2:4] + crc[0:2]  #-> Concatena a String e a tranforma em Big Endian
        data += crc_str

        return data

    @staticmethod
    def hex_para_float(hex_str: 'str')-> 'float':
        ''' Este método utiliza de modulos built-in python para 
        converter uma sequencia binaria em Float utilizando IEEE-754 '''
        
        float_final = struct.unpack('!f', bytes.fromhex(hex_str))[0]
        
        return  float_final

    @classmethod
    def enviar_comando(cls, conexao: 'socket', comando: 'int', leitora: 'int', parametro: 'str' = '00')-> 'str | None':
        ''' Monta o comando completo para envio ao 
        medidor e retorna os valores de resposta '''

        try:
            comando_hex = cls.montar_hex_comando(comando, leitora, parametro)
            comando = cls.envio_comando(conexao, comando_hex)

            try:
                resposta =  comando.result()

            except TimeoutError as t_out:
                print("Não foi recebida uma resposta dentro do tempo limite, retornando 'None'!")
                return 
            
        except Exception as e:
            print(f'Houve um erro desconhecido durante o Envio do comando!\nERRO: {e}')
            return 
        
        return resposta

    @classmethod
    def selecionar_canais_para_leitura(cls, sock: 'socket', leitora: 'int', canal: 'str')-> 'None':
        '''Envia um comando para selecionar os 
        canais a serem lidos na memoria de massa:
        00 = [1, 2, 3]      
        01 = [4, 5, 6] 
        02 = [7, 8, 9]       
        03 = [10, 11, 12]
        04 = [13, 14, 15]    
        05 = [16, 17, 18] 
        06 = [19, 20, 21] '''

        cls.enviar_comando(sock, 21 ,f'{leitora}00{canal}')
        
    @classmethod
    def receber_blocos_memoria(cls, conexao: 'socket', leitora: 'int', canal: 'str' = '01')-> 'list | None':
        ''' Retorna todos os blocos armazenados 
        na memoria de massa do medidor tratados 
        e traduzidos para Decimal '''

        try:
            blocos = []
            cls.selecionar_canais_para_leitura(conexao, leitora, canal)

            resposta = cls.enviar_comando(conexao, 26, 654321)        #-> Comando para Leitura do                            
            octetos = ComunicacaoNBR.dividir_octetos_word(resposta)   # primeiro bloco de memoria

            blocos.append(octetos)
            stop_hex = int(octetos[7])    #-> Define uma variavel 'flag' para controle de leitura

            while(stop_hex < 10):
                resposta = cls.enviar_comando(conexao, 26, 654321, '01')   #-> Comando para Leitura do      
                octetos = ComunicacaoNBR.dividir_octetos_word(resposta)    # proximo bloco de memoria

                if octetos[2] == '26':
                    blocos.append(octetos)
                    stop_hex = int(octetos[7]) 
                else:
                    while True:
                        resposta = cls.enviar_comando(conexao, 26, 654321, '02')
                        octetos = ComunicacaoNBR.dividir_octetos_word(resposta)

                        if octetos[2] == '26':
                            blocos.append(octetos)
                            stop_hex = int(octetos[7])

                            break
                        
            memoria_traduzida = cls.interpretar_dados_blocos(blocos)   #-> Recebe do método todos os dados em 
                                                                       # Decimal, de forma limpa e traduzida                                        
            return memoria_traduzida
        
        except Exception as e:
            print(f'Erro ao receber memoria de massa: {e}')
            return

    @staticmethod
    def interpretar_dados_blocos(blocos: 'list')-> 'list':
        ''' Coloca todos os blocos de memoria em apenas um,
        separa os octetos em intervalos de memoria, interpreta 
        esses intervalos e os converte para DEC natural '''

        conteudo = []
        tabela = []
        index = 1

        for bloco in blocos:
            conteudo.append(bloco[9:258])  #-> Separa somente os dados do bloco

        dados_completo = list(conteudo[0])
        conteudo.pop(0)

        for bloco in conteudo:
            for octeto in bloco:
                dados_completo.append(octeto)  #-> Constroi uma lista contendo todos 
                                               # os dados de memoria dos blocos

        for c in range(0, len(dados_completo)- 9, 9):
            #-> A leitura dos dados é feita a partir de 2 intervalos por vez
            
            intervalos = list(dados_completo[c : c + 9])
            intervalo_1 = intervalos[0:4]

            #-> Aqui ocorre uma modificação nos Bytes dos octetos <-# 
            # (Modificação descrita na documentação do modulo)
            intervalo_1.append(intervalos[4][0])
            intervalos.insert(6, f'{intervalos[4][1]}{intervalos[7][0]}')
            intervalos.append(intervalos[8][1])
            intervalos.pop(8)
            #------------------------------------------------------->
            
            intervalos = [intervalo_1, intervalos[5:]]

            for intervalo in intervalos:
                hex_canal_1 = int(f'{intervalo[1][0]}{intervalo[0]}', 16)  #-> Forma de leitura dos HEX dos 
                hex_canal_2 = int(f'{intervalo[1][1]}{intervalo[2]}', 16)  # canais descrita na documentação
                hex_canal_3 = int(f'{intervalo[4]}{intervalo[3]}', 16)
                
                tabela.append([index, hex_canal_1, hex_canal_2, hex_canal_3])
                index += 1

        return tabela

    @staticmethod
    def dividir_octetos_word(word_hex: 'str')-> 'list':
        ''' Retorna uma lista contendo octetos (HEX) de 2 Char '''

        resultado = [word_hex[index:index+2] for index in range(0, len(word_hex), 2)]

        return resultado

    @staticmethod
    def converter_para_big_endian(hex: 'list | str')-> 'list | str':
        '''Inverte a ordem dos bytes da WORD HEX
        caso a entrada seja uma lista, a ordem é simplesmente invertida
        caso a entrada seja uma String, a mesma é invertida cada HEX 2(CHAR) '''

        if isinstance(hex, list):
            hex.reverse()

            return hex
        
        else:
            temp_str = ''

            for c in range(len(hex)-2, -2, -2):
                temp_str += hex[c : c+2]

            return temp_str

